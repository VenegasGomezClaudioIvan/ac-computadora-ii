# Computadoras Electronicas

```plantuml
@startmindmap
* Computadoras Electrónicas
** Coputadoras Electromecanicas
*** Computadora Harvard Mark 1
**** Fabricante
***** IBM
**** Año de fabricacion 
***** 1994
**** Caracteristicas 
***** 765,000 componentes
***** 80 KM de cable
***** Eje de 15 m
***** Motor de 5 HP(Caballos de Fuerza)
***** 3500 Relés
****** ¿Qué es?
******* Es un interruptor mecanico controlado electricamente
****** Se compone de
******* Una Bobina
******* Brazo de hierro
******* Contactos
****** funcionamiento
******* una corriente electrica atraviesa la bobina, esta genera \nun campo magnetico, el campo magnetico atrae al brazo \nmetalico por consecuencia se unen los contactos \ncerrado el circuito
****** Desventajas
******* El brazo de hierro tiene masa por consecuencia \nentra en juego la inercia 
******** ¿Que es la inercia?
********* La opocicion que tiene los cuerpos a cambiar su estado de movimiento
****** Los reles en 1940 cambian de estados aproxiamdamente 50 veces por minuto
****** Los reles eran suceptibles al daño \npor el uso o por el timpo
******* Por estadistica fallaba un rele al dia
******* Los insectos eran atraidas a las \ncondiciones que tenian las maquinas
******* En septiembre de 1947 se saca una polilla del rele
******** Grace Hopper dijo que desde entonces cuando algo salia \ncon una computadora decian que tenian bichos(bugs)
**** Usos y aplicaciones
***** Simulacion y calculos para el proyecto Manhattan
****** Tiempo por operacion
******* 3 Sumas o restas por segundo 
******* Multiplicacion 6 segundos
******* División 15 segundos
******* Operaciones complejas o las funciones trigonometricas mas de un minuto

** Computadoras Electronicas
*** Valvula termoiónica
**** ¿Cuando se desarrollo?
***** En 1904
**** ¿Quien la desarrollo?
***** Un cientifico llamado John Ambrose Fleming  
**** componentes 
***** Filamento 
***** Dos electrodos
****** Anodo
****** Catodo
***** Vulvo de cristal sellado
**** ¿Como funciona?
***** cuando el filamento se enciende y se \ncalienta calienta permite el flujo de elctrones \n desde el anodo hacia el catodo
****** ¿Como se llama este proceso?
******* Emision termoionica
******** Permite el flujo de energia en una sola direccion
**** Avances
***** Se agregar el electrodo de control 
****** ¿Quien lo agrega? 
******* Lee De Forest
****** ¿En que año? 
******* 1906
****** ¿donde se situa? 
******* Se situaba entre el anodo y el catodo del diseño de Fleming
****** ¿Como funciona?
******* al agregar una carga positiva o negativa \nal anodo de control permite el flujo o detiene la corriente 
******** consecuencias 
********* logra la misma funcionalidad que los reles 
********** Ventajas sobre los reles
*********** elimina partes moviles 
*********** Mayor confiabilidad
************ Menor desgaste por uso
*********** Mayor velocidad Cambiaban de estado miles de veces por segundo
********** Desventajas 
**** usos
***** fueron la base para la radio 
***** llamadas a larga distancia
***** Colossus Mark 1
****** diseñado por
******* El Ingeniero Tommy Flowers
****** Finalizado en diciembre de 1943
****** lugar
******* Parque Bletchley en Reino Unido 
****** Usos
******* Ayudaba a decodificar codigo nazi
****** Componentes 1era versión
******* 1,600 tubos de vacio
****** Se construyeron 10 colossus 
****** Es la primera computadora electronica programable
******* ¿como se programaba?
******** se realizaba mediante la conexion de \ncientos de cables en un tablero

***** ENIAC 
****** significado de las siglas
******* CLACULADORA INTEGRADORA NÚMERICA ELECTRONICA
****** Fecha de construccion 
******* 1946
****** lugar de construccion
******* Universidad de Pensylvania
****** Diseñada por
******* John Mauchly y J. Presper Eckert
****** Fue la primera computadora programable de proposito general
****** tiempos por operacion
******* podia realizar 5,000 sumas y restas de 10 digitos por segundo
****** Opero durante 10 años
****** Desventajas
******* Era operacional durante la mitad del dia antes de dañarse


**** Desventajas
***** Eran demasiado caros
***** eran fragiles
***** se podrian quemar

*** Transistor 
**** Cradores
***** John Bardeen, Walter Brattain y WIlliam Shockley
**** fue creado en los laboratorios Bell en 1947
**** El transistor tiene la misma funcion \nde un tubo de vacio o un rele, es \nun interruptor electrico 
**** ¿De que esta hecho?
***** Esta hecho de silicio con otro compuesto(Dopaje) que permite el paso de corriente electrica 
****** Depende del elemeto que se le agregue al silicio \neste puede tener exceso o carencia de elctrones
**** usos
***** gracias a los transistores se pudo construir la IBM 608 en 1957
**** Ventajas 
***** Reducir costes 
***** Aumentar velocidad 
***** Aumentar la confiabilidad

@endmindmap
```

# Arquitectura Von Neumann y Arquitectura Harvard
```plantuml
@startmindmap
* Arquitectura Von Neumann y Arquitectura Harvard
** Arquitectura Von Neumann
*** Descrita en 1945 por el matematico y fisico John Von Neuman y otros 
*** Modelo
**** Los datos y programas se almacenan en una misma memoria de lectura-escritura 
**** Los contenidos de esta memoria se acceden indicando su posicion sin importar su tipo
**** Ejecucion en secuencia
**** Representacion binaria
*** Contiene 
**** Unidad central de procesamiento 
***** Unidad de control
***** Unidad logica aritmetica
***** Registros
**** memoria principal
***** almacena datos e instrucciones
**** sistema de E/S

*** surge el concepto programa almacenado 
*** la separacion de la memoria y la CPU creo \nun problema llamado Nuemann bottleneck (cuello de botella de Neumann) 
**** ¿Que es?
***** se refiere a que la cantidad de datos que pasa entre estos componentes no es igual a sus velocidades
*** Buses
**** La comunicacion es atraves de buses 
***** Bus de datos
***** Bus de direcciones 
***** Bus de control 
*** Instrucciones
**** Los programas se encuentran localizados en memoria y consisten de instrucciones
**** La CPU se encarga de ejecutar las instrucciones  atraves de ciclo de instrucción
**** Las instrucciones son secuencias de 1 y 0
***** Son 
****** Codigo maquina
**** las instrucciones se puede codificar en lenguaje ensamblador o lenguales de programacion
**** Una CPU que opera a 3 GHz puede ejecutar 3 000 000 000 de operaciones por segundo
*** Ciclos de ejecución 
**** Se obtiene la siguiente instruccion
**** se incrementa el PC 
**** Se decodifica la instruccion
**** obtiene de memoria los operandos necesarios
**** La ALU ejecuta y deja los resultados en registros o en memoria

** arquitectura Harvard
*** Modelo 
**** Se referia a las arquitecturas de computadoras que utilizaban \ndispositivos de almacenamiento fisicamente separados \npara las instrucciones y para datos
*** Contiene
**** describe una arquitectura de un computador digital electronico con partes que constan de:
***** Unidad de procesamiento logico
****** contiene  
******* Unidad logica aritmetica 
******* Unidad de control(UC)
******** El UC lee la instruccion, genera las señales de \ncontrol para obetener los operandos y luego ejecuta \nla instruccion mediante la ALU 
******* Registros
***** Memoria principal
****** Memoria de instrucciones 
******* Almacena instrucciones que ejecutara el microcontrolador
******* el tamaño de las palabras  se adapta al numero \nde bits de las instrucciones del microcontrolador
******* se implementa utilizando memorias no volatiles
******** ROM
******** PROM
******** EPROM
******** EEPROM O flash
****** Memoria de datos
******* Almacena los datos utilizados por los programas
******* Los datos varian constantemente por eso utiliza memoria Ram  
******* habitualmete se utiliza memoria SRAM
***** sistema de E/S 

*** Memorias
**** Memorias rapidas tienen un costo elevado
**** Memoria cache 
***** los datos que necesita el procesador estan en el cache lo que mejora el rendimiento
*** solucion Harvard
**** Las instrucciones y datos se almacenan en \ncaches separadas para mejorar el rendimiento
**** Desventajas
***** al dividir la memoria cache en 2 el \nrendimiento es mejor cuando la frecuencia de lectura de \ninstrucciones y de datos es similar


@endmindmap
```

# Basura Electronica 

```plantuml
@startmindmap
* Basura Electrónica
** terminales, circuitos y procesadores
*** Materiales que se pueden extraer
**** Oro
**** Plata
**** Cobre 
**** cobalto
** ¿como se deshecha?
*** la basura electronica se transporta a un pais \ntercermundista donde se extren los metales preciosos
**** Principales pais donde se exporta
***** china
***** India
***** México
*** La basura electronica muchas veces se transporta de manera ilegal
** Medio Ambiente
*** Los desechos que no se pueden sacar provecho finaiceramente se desechan a rios, suelos y provocan una contaminacion ambiental
** Reciclaje 
*** El reciclaje de basura electronica es costoso y complicado 


@endmindmap
```
